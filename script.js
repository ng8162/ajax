let ajaxBtn = document.getElementById('ajaxbtn')
ajaxBtn.addEventListener('click',makeRequest)
var xhr =new XMLHttpRequest()
/*
*Recuperation de tous les paragraphes
*/
/*Solution 1*/
var p = document.getElementsByTagName('p')
let object = Object.keys(p)
object.forEach(element=>{
    console.log(p[element])
})
/*Solution 2 */
for (let index = 0; index < p.length; index++) {
    element2 = p[index]
    console.log(element2.innerHTML)
    
}

var form = document.getElementById('formauteur')
form.addEventListener('submit',makeRequest)

function makeRequest(e){
    e.preventDefault()//evite la soumission de formulaire et de changer de page
    console.log('form',form)

    let firstname = document.getElementById('nom').value
    let name = document.getElementById('prenom').value

    var params = 'nom=' + firstname + '&prenom=' + name

    console.log('makeRequest()')
    
    //changement d'etat de xhr
    xhr.onreadystatechange = alertContent
    let url = 'test.php'
    xhr.open('POST', url)
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(params)

    function alertContent(){
        //quand xhr est terminée (DONE)
        if (xhr.readyState === XMLHttpRequest.DONE) {
            console.log(xhr.responseText)
           //alert(xhr.responseText) 
           let divResponse1 = document.getElementById('response1')
           let divResponse2 = document.getElementById('response2')

           let object=JSON.parse(xhr.responseText)

           console.log(object)

           //Solution 1
            let text= ''
            for (const key in object) {
                console.log(key + ': '+ object[key]);
                 text = text + key + ': '+ object[key] + '<br>'
            }

            //Solution 2
            var string=''
            Object.keys(object).forEach(function (k) {
                string += "<p>" + k + ' - ' + object[k] + "</p>";
                });

            divResponse1.innerHTML= 'Solution 1 - for in<br>' + text//soluton 1
            divResponse2.innerHTML= '<br>Solution 2 forEach' + string//solution 2
            
        }           
        
    }
}
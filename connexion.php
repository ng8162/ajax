<?php

 function connectDB(){
    include 'config.php';
    $config = new Config();
    $host = $config->getHost();
    $dbname = $config->getDbname();
    $login = $config->getLogin();
    $password = $config->getPassword();

    try {

        $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;

        // On se connecte Ã  MySQL
        $bdd=new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", "$login", "$password", array(PDO::ATTR_PERSISTENT => TRUE));
        
        
        
    } catch (Exception $e) {
        // En cas d'erreur, on affiche un message et on arrÃªte tout
        die('Erreur de connexion a votre base de donnees: ' . $e->getMessage());
        //$e->getMessage()) est a garder uniquement en phese de test pour ne afficher les informations
    }
 return $bdd;
}
?>